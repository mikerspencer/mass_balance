# ------------------------------------------
# ------------------------------------------
# Prepare precipitation data
# ------------------------------------------
# ------------------------------------------

# ------------------------------------------
# Setup
# ------------------------------------------
setwd("~/Copy/Michael/Uni_temp/Mass_balance")
library(ncdf4)
library(sp)
library(rgdal)
library(parallel)
library(dplyr)
source("./repo/functions.R")

# ------------------------------------------
# Define required cells
# ------------------------------------------
cells = readOGR("./data/spatial/12007_cell.shp", "12007_cell")
cells = data.frame(cells)[,c(8, 9, 16)]
names(cells)[1:2] = c("x", "y")

# ------------------------------------------
# Read precip and prep for each cell
# ------------------------------------------
Dates = data.frame(year=1982:2010, day0=as.numeric(strftime(paste0(1982:2010, "-10-01"), "%j")), days1=as.numeric(strftime(paste0(1983:2011, "-09-30"), "%j")), days.total=as.numeric(as.Date(paste0(1983:2011, "-09-30")) - as.Date(paste0(1982:2010, "-10-01"))))

precip = mclapply(1:nrow(Dates), mc.cores=4, function(i){
   # Iterate over cells
   nc.dat = lapply(1:nrow(cells), function(j){
      x = nc_reader(Dates[i, "year"], cells[j, "x"], cells[j, "y"], Dates[i, "day0"], Dates[i, "days1"])
      # Volume of water from cell
      x$vol = (x$Precip / 1000) * cells[j, "area"]
      # Output
      data.frame(year=Dates[i, "year"], x=cells[j, "x"], y=cells[j, "y"], precip=sum(x$vol), temp=min(x$meantemp), days0=sum(x$meantemp<0))
   })
   do.call("rbind.data.frame", nc.dat)
})
precip = do.call("rbind.data.frame", precip)


# ------------------------------------------
# Get total precip per year
# ------------------------------------------
precip = summarise(group_by(precip, year), PrecipPA=sum(precip), TempPA=min(temp), DaysCold=mean(days0))

# ------------------------------------------
# Tidy
# ------------------------------------------
rm(cells, Dates)
