---
title: "Mass balance at Mar Lodge"
author: "Mike Spencer"
date: "30 September 2015"
output: 
  html_document: 
    toc: yes
---

Using CEH GEAR precipitation data and gauged daily flow data this document looks at the water balance for the Mar Lodge gauge (12007 <http://nrfa.ceh.ac.uk/data/station/info?12007=>) on the River Dee. The catchment is 289 km^2.

```{r setup, echo=FALSE, message=F}
setwd("~/Copy/Michael/Uni_temp/Mass_balance/")
source("repo/flow.R")
source("repo/precip.R")
options(scipen=999)
```

```{r merge, echo=FALSE}
df = merge(precip, flow, by.x="year", by.y="wy")
# Convert volumes to Mm3 pa
df$PrecipPA = round(df$PrecipPA / 1000000)
df$FlowPA = round(df$FlowPA / 1000000)
```


## Compare flow and precipitation

### Time series plots

```{r time, echo=FALSE}
plot(PrecipPA ~ year, df, type="l", lwd=2, lty=2, ylab="Precipitation Mm^3 pa", xlab="Water year", ylim=c(280, 510))
lines(FlowPA ~ year, df, type="l", lwd=2, ylab="River flow Mm^3 pa", xlab="Water year")
legend("bottomleft", c("Precipitation", "River flow"), lwd=2, lty=c(2, 1))
```

### Scatter plot of flow and precipitation

```{r scatter, echo=FALSE, fig.height=6}
# Set colours
pal = colorRampPalette(c("blue", "red"))
df$order = findInterval(df$DaysCold, sort(df$DaysCold))

plot(FlowPA ~ PrecipPA, df, pch=19, col=pal(nrow(df))[df$order], xlab="Precipitation pa (m^3)", ylab="Flow pa (m^3)")
abline(0, 1, lty="dotted")
abline(lm(FlowPA ~ PrecipPA, df))
legend("bottomright", legend=c(paste(round(range(df$DaysCold)), "days below 0C"), "1:1 line", "Linear regression"), lty=c(NA, NA, "dotted", "solid"), pch=c(19, 19, NA, NA), col=c("red", "blue", "black", "black"))
```

### Percentage difference

Showing precipitation as a percentage of flow.

```{r Perc diff, echo=FALSE}
x = df$PrecipPA / df$FlowPA
hist(x, main="Precipitation as a percentage of flow", breaks=7)

# Time
plot(x ~ df$year, main="Precipitation as a percentage of flow through time", xlab="Year", ylab="% of accounted water")

# Related to flow?
plot(x ~ df$FlowPA, main="Precipitation as a percentage of flow against flow", xlab="Annual flow", ylab="% of accounted water")

# Related to days below 0C?
plot(x ~ df$DaysCold, main="Precipitation as a percentage of flow against days below 0C", xlab="Days below 0C pa", ylab="% of accounted water")
```

Using days below 0C as a proxy for snow; there is a small decrease in precipitation compared to flow. This could be wind blown snow coming off the Cairngorms contributing to the upper Dee catchment.

Using Kay and Davis (2008) we can estimate potential evapotranspiration for the area:

```{r PE, echo=F}
# Read PE
PE = read.csv("../data/spatial/12007_PE.csv")
PE = rowSums(PE[, 5:16])
hist(PE, main="Potential annual evapotranspiration in Mar Lodge catchment", xlab="mm per annum")
```

Potential evapotranspiration is approx `r signif(median(PE), 2)` mm per annum, this is an extra `r signif(289 * (median(PE) / 1000), 3)` Mm^3/yr or `r signif(100 * (289 * (360 / 1000)) / median(df$FlowPA), 2)`% of the mean annual discharge.

I've not looked up values of groundwater flow or residence times for the Mar Lodge catchment, but these are a further source of uncertainty.

CEH GEAR data have no lapse rate applied. In Burt & Holden (2010) they estimate precipitation gradients of 10-20 mm/yr/km. Adding this orographic enhancement to the higher GEAR cells would likely make for a more realistic water balance. 

It would be interesting to see how closely we could match the orographically enhanced CEH GEAR data with flow. In the short term, it can be concluded that CEH GEAR precipitation is a median of `r round(100 * median(df$PrecipPA / (df$FlowPA + (289 * (median(PE) / 1000)))))`% river flow, including potential evapotranspiration.