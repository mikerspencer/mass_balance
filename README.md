## Mass Balance

Takes CEH National River Flow Archive (NRFA) and GEAR (precipitation) data to understand the mass balance of water in Scotland. More specifically, highlight a potential underestimation of precipitation. This is likely to be through missing snowfall/redistribution and via a flat interpolation of precipitation data, i.e. no account of a precipitation lapse rate.
